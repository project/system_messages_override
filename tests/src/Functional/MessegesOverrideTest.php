<?php

namespace Drupal\Tests\system_messages_override\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the general functionality of the module.
 *
 * @group system_messages_override
 */
class MessegesOverrideTest extends BrowserTestBase {

  public $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system_messages_override'];

  /**
   * A user with permission to administer system messages override config.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->strictConfigSchema = FALSE;
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer system messages override config']);
  }

  /**
   * Tests the general functionality of the module.
   */
  public function testOverrides() {
    $this->assertTrue(TRUE);
    $config_route_url = Url::fromRoute('system_messages_override.configurations');
    $front_page = Url::fromRoute('<front>');

    // Navigate to the configurations route. Should result in a 403.
    $this->drupalGet($config_route_url);
    $this->assertSession()->statusCodeEquals(403);

    // Create a message and navigate to the front page.
    \Drupal::messenger()->addStatus('This is a static text');
    $this->drupalGet($front_page);
    $this->assertSession()->statusCodeEquals(200);
    // Make sure the message is here.
    $this->assertSession()->pageTextContains('This is a static text');

    // TODO: Continue the test.
  }

}

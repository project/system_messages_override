<?php

namespace Drupal\system_messages_override;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class SystemMessagesOverrideMessenger.
 */
class SystemMessagesOverrideMessenger extends Messenger {

  use StringTranslationTrait;

  /**
   * The drupal logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Gets the logger channel.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   */
  public function getLogger() {
    return $this->logger ?? \Drupal::logger('system_messages_override');
  }

  /**
   * Sets the logger channel.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger channel.
   *
   * @return $this
   *   The SystemMessagesOverrideMessenger object.
   */
  public function setLogger(LoggerChannelInterface $logger_channel) {
    $this->logger = $logger_channel;
    return $this;
  }

  /**
   * Gets the config factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   */
  public function getConfigFactory() {
    return $this->configFactory ?? \Drupal::service('config.factory');
  }

  /**
   * Sets the config factory interface.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *
   * @return $this
   *   The SystemMessagesOverrideMessenger object.
   */
  public function setConfigFactory(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    return $this;
  }

  /**
   * Check if the message is the message to alter.
   *
   * @param mixed $drupal_message
   *   The original drupal message.
   * @param string $compare_to
   *   The string to compare to.
   * @param string $replace_with
   *   The string to replace with.
   *
   * @return string|TranslatableMarkup|Markup|false
   *   return string or an object that implements '__toString' to change the message, FALSE if not.
   */
  protected function alterMessage($drupal_message, $compare_to, $replace_with) {
    // Check if $drupal_message is a TranslatableMarkup object.
    if ($drupal_message instanceof TranslatableMarkup) {
      // Change the way to compare.
      $untranslated_string = $drupal_message->getUntranslatedString();

      if ($compare_to === $untranslated_string) {
        // Replace the string inside TranslatableMarkup object.
        return $this->t($replace_with, $drupal_message->getArguments(), $drupal_message->getOptions());
      }
    }

    // Check if the $drupal_message is an object which implements __toString.
    if (method_exists($drupal_message, '__toString')) {
      /** @var \Stringable $drupal_message */
      $drupal_message = $drupal_message->__toString();

      if ($drupal_message === $compare_to) {
        return Markup::create($this->t($replace_with));
      }
    }

    // Lastly, check if the two values are same exactly.
    if ($drupal_message === $compare_to) {
      return $replace_with;
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function addMessage($message, $type = self::TYPE_STATUS, $repeat = FALSE) {
    $config = $this->getConfigFactory()->get('system_messages_override.settings');
    $debug = $config->get('debug') ?? FALSE;
    // Alter the message before sending it.
    $messages_override = $config->get('messages_override') ?? [];

    // Log the message so we can ease the process of trying to find the exact message string.
    if ($debug) {
      $this->getLogger()->debug(Html::escape($message instanceof TranslatableMarkup ? $message->getUntranslatedString() : $message));
    }

    // Try to find if the message is inside.
    foreach ($messages_override as $override_message) {
      // Use try catch to prevent any major errors on the website, and use the logger.
      try {
        if ($new_message = $this->alterMessage($message, $override_message['original'], $override_message['new'])) {
          $message = $new_message;
          break;
        }
      }
      catch (\Exception $e) {
        // If debug is enabled, use core messenger to add an error message, so we avoid introducing an infinite loop. Also, create a log.
        if ($debug) {
          \Drupal::service('core_messenger')->addError($this->t('Failed to override the message. @exception', ['@exception' => $e->getMessage()]));
          $this->getLogger()->error($e->getMessage());
        }
      }
    }

    // Call the parent 'addMessage'.
    return parent::addMessage($message, $type, $repeat);
  }

}

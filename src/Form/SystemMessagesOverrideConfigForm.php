<?php

namespace Drupal\system_messages_override\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SystemMessagesOverrideConfigForm.
 */
class SystemMessagesOverrideConfigForm extends ConfigFormBase implements TrustedCallbackInterface {

  /**
   * The uuid generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   *
   */
  public static function trustedCallbacks() {
    return ['moveRows'];
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->uuid = $container->get('uuid');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'system_messages_override_config_form';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['system_messages_override.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('system_messages_override.settings');
    $messages = $form_state->get('messages');
    if (!is_array($messages)) {
      $messages = $config->get('messages_override') ?? [];
      $form_state->set('messages', $messages);
    }

    $header = [$this->t('Original'), $this->t('New'), $this->t('Remove')];
    $form['ajax_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'ajax-wrapper'],
    ];

    $form['ajax_wrapper']['messages_override'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => [],
      '#empty' => $this->t('No messages to override.'),
      '#attributes' => [
        'id' => 'drupal-messages-override-config-form',
      ],
    ];

    $form['ajax_wrapper']['rows']['#tree'] = TRUE;
    // We add the rows outside the the table first, because, #ajax does not work inside tables, we later move them to the table using a #pre_render.
    $rows = &$form['ajax_wrapper']['rows'];
    foreach ($messages as $uuid => $message) {
      $rows[$uuid]['original'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Original'),
        '#default_value' => $message['original'] ?? NULL,
        '#required' => TRUE,
      ];

      $rows[$uuid]['new'] = [
        '#type' => 'textarea',
        '#title' => $this->t('New'),
        '#default_value' => $message['new'] ?? NULL,
        '#required' => TRUE,
      ];

      $rows[$uuid]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => 'remove' . $uuid,
        '#submit' => ['::removeMessage'],
        '#limit_validation_errors' => [],
        '#attributes' => [
          'data-uuid' => $uuid,
        ],
        '#ajax' => [
          'callback' => [$this, 'updateMessagesContainer'],
          'wrapper' => 'ajax-wrapper',
        ],
      ];
    }

    $form['actions']['add'] = [
      '#type' => 'submit',
      '#value' => 'Add Message Override',
      '#submit' => ['::addMessage'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'updateMessagesContainer'],
        'wrapper' => 'ajax-wrapper',
      ],
    ];

    $form['ajax_wrapper']['#pre_render'][] = [$this, 'moveRows'];

    // Add checkbox for debugging.
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('This will log any drupal message so you can accuratly get the desired messages to override'),
      '#default_value' => $config->get('debug'),
    ];

    return $form;
  }

  /**
   * This will move the remove button from outside to the inside of the table.
   *
   * @param array $wrapper
   *
   * @return void
   */
  public function moveRows(array $wrapper) {
    if (isset($wrapper['rows'])) {
      $children = Element::children($wrapper['rows']);
      foreach ($children as $uuid) {
        $old_element = $wrapper['rows'][$uuid];
        $wrapper['messages_override']['#rows'][] = [
          'original' => ['data' => $old_element['original']],
          'new' => ['data' => $old_element['new']],
          'remove' => ['data' => $old_element['remove']],
        ];
      }

      unset($wrapper['rows']);
    }

    return $wrapper;
  }

  /**
   * Adds a message to the form state.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function addMessage($form, FormStateInterface $form_state) {
    $messages = $form_state->get('messages') ?? [];
    $messages[$this->uuid->generate()] = [
      'original' => '',
      'new' => '',
    ];
    $form_state->set('messages', $messages)->setRebuild();
  }

  /**
   * Removes a message from the form state.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function removeMessage($form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $uuid = $triggering_element['#attributes']['data-uuid'];
    $messages = $form_state->get('messages') ?? [];
    if (isset($messages[$uuid])) {
      unset($messages[$uuid]);
    }

    $form_state->set('messages', $messages)->setRebuild();
  }

  /**
   * Reupdates the 'ajax_wrapper' div element using ajax.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function updateMessagesContainer($form, FormStateInterface $form_state) {
    return $form['ajax_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $config = $this->config('system_messages_override.settings');
    $values = $form_state->getValues();
    $values['messages_override'] = $values['rows'] ?? NULL;
    unset($values['rows']);
    foreach ($values as $id => $value) {
      $config->set($id, $value);
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\system_messages_override;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * This class will alter core's messenger service.
 */
class SystemMessagesOverrideServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    // Override core's messenger service.
    if ($container->hasDefinition('messenger')) {
      $messenger = $container->getDefinition('messenger');
      $core_messenger = clone $messenger;
      $messenger->setClass(SystemMessagesOverrideMessenger::class);

      // Re-add the core messenger so we can use it in case any Exception happened to our messenger.
      $container->setDefinition('core_messenger', $core_messenger);
    }
  }

}

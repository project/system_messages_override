# System Messages Override

This module will override Drupal's core messenger with a custom messenger, which will overrides Drupal's messages depending on the configurations of the modules (/admin/config/system/messages-override). It also works with TranslatableMarkup objects.

## Usage Examples

  Inside the module configurations form, you add a message override, then fill in the **original** and the **new** messages.

|Code|Original|New|Result|
|--|--|--|--|
| ->addMessage("Static Message") |Static Message| New Static Message |->addMessage(t("New Static Message"))
|addMessage(t("Dynamic text: @number"))|Dynamic text: @number|New Message Foo: @number|addMessage(t("New Message Foo: @number",["@number" => $some_number]))
|addMessage(Markup::create("This is a <a  href='#Test'>Link</a>")|This is a <a  href='#Test'>Link</a>|New <a  href='#Test'>Link</a>|addMessage(t("New <a  href=\\"#Test\\">Link</a>"))

## Debugging

You can check the "Debug" option with "dblog" module in the configurations to enable logging of exact messages so you can easily copy them and override them.
